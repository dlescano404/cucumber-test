import { Given, Then, When } from "@cucumber/cucumber";
import { expect } from "chai";

let suma: any = null
let result: number|null = null

Given('creo la funcion sumar', function () {
  suma = (a: number, b: number) => (a+b)
});

When('tomo el numero {int} y {int} y los sumo', function (int: number, int2: number) {
  result = suma(int, int2)
});


Then('el resultado tiene que ser {int}', function (int) {
  expect(result).to.be.equal(int)
});